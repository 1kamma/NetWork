using System;
using System.Runtime.InteropServices;
using CoreAudioApi;

class Program
{
    static void Main()
    {
        // Mute the computer
        MuteComputer();

        // Wait for user input to exit
        Console.WriteLine("Press any key to exit...");
        Console.ReadKey();
    }

    static void MuteComputer()
    {
        // Create an instance of the MMDeviceEnumerator
        var enumerator = new MMDeviceEnumerator();

        // Get the default audio render device (speaker)
        var device = enumerator.GetDefaultAudioEndpoint(EDataFlow.eRender, ERole.eMultimedia);

        // Mute the audio
        device.AudioEndpointVolume.Mute = true;
    }
}
