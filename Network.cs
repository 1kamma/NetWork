using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;

class Program
{
    [DllImport("iphlpapi.dll", CharSet = CharSet.Auto)]
    public static extern int GetAdaptersInfo(IntPtr pAdapterInfo, ref int pOutBufLen);

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct IP_ADAPTER_INFO
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string AdapterName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string Description;
        public uint AddressLength;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] Address;
        public int Index;
        public uint Type;
        public uint DhcpEnabled;
        public IntPtr CurrentIpAddress;
        public IP_ADDR_STRING IpAddressList;
        public IP_ADDR_STRING GatewayList;
        public IP_ADDR_STRING DhcpServer;
        public bool HaveWins;
        public IP_ADDR_STRING PrimaryWinsServer;
        public IP_ADDR_STRING SecondaryWinsServer;
        public uint LeaseObtained;
        public uint LeaseExpires;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct IP_ADDR_STRING
    {
        public IntPtr Next;
        public IP_ADDRESS_STRING IpAddress;
        public IP_ADDRESS_STRING IpMask;
        public int Context;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct IP_ADDRESS_STRING
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
        public string Address;
    }

    [DllImport("iphlpapi.dll")]
    public static extern int SendARP(uint destIp, uint srcIp, byte[] macAddress, ref uint physicalAddrLen);

    public static string GetIP()
    {
        string hostName = Dns.GetHostName();
        IPAddress[] addresses = Dns.GetHostAddresses(hostName);
        IPAddress ipv4 = addresses.FirstOrDefault(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
        return ipv4?.ToString();
    }

    public static string GetSSID()
    {
        string ssid = "";
        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
            Process process = new Process();
            process.StartInfo.FileName = "netsh";
            process.StartInfo.Arguments = "wlan show interfaces";
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;

            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            int ssidIndex = output.IndexOf("SSID", StringComparison.OrdinalIgnoreCase);
            if (ssidIndex >= 0)
            {
                int startIndex = output.IndexOf(':', ssidIndex) + 1;
                int endIndex = output.IndexOf('\n', startIndex);
                ssid = output.Substring(startIndex, endIndex - startIndex).Trim();
            }
        }

        return ssid;
    }

    public static string GetMAC()
    {
        NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
        foreach (NetworkInterface networkInterface in networkInterfaces)
        {
            if (networkInterface.OperationalStatus == OperationalStatus.Up && networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback)
            {
                PhysicalAddress macAddress = networkInterface.GetPhysicalAddress();
                byte[] bytes = macAddress.GetAddressBytes();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    sb.AppendFormat("{0}", bytes[i].ToString("X2"));
                    if (i != bytes.Length - 1)
                        sb.Append(":");
                }
                return sb.ToString();
            }
        }
        return null;
    }

    public static void StopPrograms()
    {
        // Add code to stop programs here
        Console.WriteLine("Stopping programs...");
    }

    public static void StartPrograms()
    {
        // Add code to start programs here
        Console.WriteLine("Starting programs...");
    }

    static void Main()
    {
        string ip = GetIP();
        Console.WriteLine("IP Address: " + ip);

        string ssid = GetSSID();
        Console.WriteLine("SSID: " + ssid);

        string mac = GetMAC();
        Console.WriteLine("Ethernet MAC Address: " + mac);

        Console.WriteLine("Enter 'stop' to stop programs or 'start' to start programs:");
        string input = Console.ReadLine();

        switch (input.ToLower())
        {
            case "stop":
                StopPrograms();
                break;
            case "start":
                StartPrograms();
                break;
            default:
                Console.WriteLine("Invalid input");
                break;
        }
    }
}
